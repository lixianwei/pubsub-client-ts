## Pub/Sub Client

### Example
```typescript
import {createWSClient} from 'pubsub-client-ts';

const client = createWSClient('localhost:5001');
client.connect().then(() => {
    // rece msg
    client.recv((err, msg) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log(msg);
    });
    // ping
    setInterval(async () => {
        return client.ping()
    }, 2000);
}).catch(e => {
    console.error(e);
});
```