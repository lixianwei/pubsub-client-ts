export enum Cmd {
    Ping = 'ping',
    Pong = 'pong',
    Sub = 'sub',
    Unsub = 'unsub',
    Pub = 'pub'
}

export interface Message {
    cmd: Cmd;
    topics?: string[];
    data?: string;
}
