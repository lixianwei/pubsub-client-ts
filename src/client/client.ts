import { Message } from "../protocol";


export type MsgHandler = (err: Error | null, msg?: Message) => void;

export interface IClient {
	close(): Promise<void>;
	ping(): Promise<void>;
	pong(): Promise<void>;
	sub(topics: string[]): Promise<void>;
	unsub(topics: string[]): Promise<void>;
	pub(topics: string[], data: string): Promise<void>;
	recv(handler: MsgHandler): void;
}
