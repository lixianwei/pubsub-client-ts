import WebSocket from 'ws';
import { Message, Cmd } from "../protocol";
import { IClient, MsgHandler } from './client';

export class WSClient implements IClient {
    private ws: WebSocket | null = null;
    private msgHandler: MsgHandler = (err: Error | null, msg?: Message) => { };
    constructor(private host: string) {
    }

    connect(): Promise<WebSocket> {
        return new Promise((resolve, reject) => {
            const ws = new WebSocket(`ws://${this.host}/upgrade`);
            ws.on('open', () => {
                this.ws = ws;
                resolve(ws)
            });
            ws.on('error', function (err) {
                reject(err);
            });
            ws.on('message', (data: string) => {
                try {
                    const msg = JSON.parse(data);
                    this.msgHandler(null, msg);
                } catch (err) {
                    this.msgHandler(err);
                }
            });
        });
    }

    async ping() {
        return this.send({cmd: Cmd.Ping});
    }

    async pong() {
        return this.send({cmd: Cmd.Pong});
    }

    async sub(topics: string[]) {
        return this.send({ cmd: Cmd.Sub, topics });
    }

    async unsub(topics: string[]) {
        return this.send({ cmd: Cmd.Unsub, topics });
    }

    async pub(topics: string[], data: string) {
        return this.send({ cmd: Cmd.Pub, topics, data });
    }

    async close(){
        if (this.ws) {
            this.ws.close();
        }
    }

    recv(handler: MsgHandler): void {
        this.msgHandler = handler;
    }

    private send(msg: Message) {
        return new Promise<void>((resolve, reject) => {
            if (this.ws === null) {
                return reject(new Error('conn not ready'));
            }
            this.ws.send(JSON.stringify(msg), (err) => {
                if (err) {
                    return reject(err);
                }
                resolve();
            });
        });
    }
}